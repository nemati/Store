package com.domain.myapplication.fake_Data;

import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.Model.CourseMain;

import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class FakeCategoryData {

    public static List<CategoryMain> fill() {
        List<CategoryMain> categoryMains = CategoryMain.listAll(CategoryMain.class);

        if (categoryMains != null && categoryMains.size() > 0)
            return categoryMains;
        fillSampleData();
        return CategoryMain.listAll(CategoryMain.class);
    }


    private static void fillSampleData() {
        CategoryMain main = new CategoryMain("#E55801", "http://faradars.org/wp-content/uploads/2013/12/icon-programming.png");
        CategoryMain main2 = new CategoryMain("#E89704", "http://faradars.org/wp-content/uploads/2016/01/oloom.png");
        CategoryMain main3 = new CategoryMain("#2E0BAA", "http://faradars.org/wp-content/uploads/2016/01/olumtajrobi.png");
        CategoryMain main4 = new CategoryMain("#E70E83", "http://faradars.org/wp-content/uploads/2013/12/icon-general.png");
        main.save();
        main2.save();
        main3.save();
        main4.save();
    }
}
