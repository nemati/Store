package com.domain.myapplication.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.domain.myapplication.Model.BannerMain;
import com.domain.myapplication.R;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class BannerAdapter extends AbstractItem<BannerAdapter, BannerAdapter.ViewHolder> {

    BannerMain data;
    Context context;


    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public BannerAdapter(BannerMain data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.anishtan_layout_banner;

    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_banner_main;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);
        try {
            holder.linearLayout.setBackgroundColor(Color.parseColor(data.getColor()));
        } catch (Exception e) {
            holder.linearLayout.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        }


        Glide
                .with(context)
                .load(data.getUrlPic())
                .fitCenter()
                .into(holder.banner);
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.anishtan_iv_banner)
        ImageView banner;
        @Bind(R.id.anishtan_item_lin_banner)
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
