package com.domain.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.domain.myapplication.CheckoutSingleton;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.R;
import com.domain.myapplication.enums.TypeCourse;
import com.domain.myapplication.helper.ToFarsiChar;
import com.domain.myapplication.listener.RemoveCheckout;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.mikepenz.materialize.color.Material;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CourseGridAdapter extends AbstractItem<CourseGridAdapter, CourseGridAdapter.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();
    CourseMain data;
    Context context;
    TypeCourse typeCourse;

    private RemoveCheckout listener;

    public CourseGridAdapter(CourseMain data, Context context, TypeCourse typeCourse) {
        this.data = data;
        this.context = context;
        this.typeCourse = typeCourse;


    }

    public CourseGridAdapter(CourseMain data, Context context, TypeCourse typeCourse, RemoveCheckout listener) {
        this.data = data;
        this.context = context;
        this.typeCourse = typeCourse;
        this.listener = listener;


    }

    public CourseMain getData() {
        return data;
    }

    @Override
    public int getType() {

        return R.id.fastadapter_radiobutton_sample_item_id;
    }

    @Override
    public int getLayoutRes() {
        if (typeCourse == TypeCourse.LINEAR)
            return R.layout.item_course_lin;
        return R.layout.item_course_grid;
    }


    @Override
    public void bindView(final ViewHolder holder) {
        super.bindView(holder);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displaymetrics);

        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        switch (typeCourse) {
            case GRID:
                Glide
                        .with(context)
                        .load(data.getUrl())
                        .placeholder(Color.WHITE)
                        .fitCenter()
                        .override(width / 2, height / 2)
                        .dontAnimate()
                        .into(holder.pic);
                break;
            case LINEAR:
                Glide
                        .with(context)
                        .load(data.getUrl())
                        .placeholder(Color.WHITE)
                        .fitCenter()

                        .into(holder.pic);


        }


        Glide
                .with(context)
                .load(data.getUrl())
                .placeholder(Color.WHITE)
                .fitCenter()
                .override(width / 2, height / 2)
                .into(holder.pic);


        holder.main.setText(data.getMatn());
        holder.aut.setText(ToFarsiChar.farsichar(data.getCount()));
        holder.bar.setRating(data.getTank());
        if (data.getStrikePrice() != null && !data.getStrikePrice().isEmpty()) {


            SpannableString text = new SpannableString(ToFarsiChar.farsichar(data.getStrikePrice() + " " + data.getPrice()));
            text.setSpan(new StrikethroughSpan(), 0, data.getStrikePrice().length(), 0);
            text.setSpan(new ForegroundColorSpan(context.getResources().getColor(android.R.color.holo_red_light)), 0, data.getStrikePrice().length(), 0);
            text.setSpan(new RelativeSizeSpan(.75f), 0, data.getStrikePrice().length(), 0);
            holder.price.setText(text, TextView.BufferType.SPANNABLE);
        } else {

            holder.price.setText(ToFarsiChar.farsichar(data.getPrice()));
        }


        Typeface type = Typeface.createFromAsset(context.getAssets(), "font/IRAN Sans Light.ttf");

        holder.main.setTypeface(type);
        holder.price.setTypeface(type);
        holder.aut.setTypeface(type);
        holder.bar.setDrawingCacheBackgroundColor(context.getResources().getColor(R.color.digikala_adrs_title1));

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "به سبد خرید اضافه شد", Toast.LENGTH_SHORT).show();
                CheckoutSingleton.getInstance().add(data);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(context)
                        .title("حذف محصول")
                        .content("آیا از حذف این محصول اطمینان دارید؟")
                        .positiveText("بله")
                        .negativeText("خیر")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                Toast.makeText(context, "ایتم حذف شد", Toast.LENGTH_SHORT).show();
                                if (listener != null)
                                    listener.onItemRemoved(data);
                            }
                        })
                        .show();
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(data.getCount()) - 1 == 0)
                    return;
                data.setCount(Integer.parseInt(data.getCount()) - 1);
                holder.aut.setText(ToFarsiChar.farsichar(data.getCount()));
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                data.setCount(Integer.parseInt(data.getCount()) + 1);
                holder.aut.setText(ToFarsiChar.farsichar(data.getCount()));
            }
        });


    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.discoverCourseBoxCourseImageView)
        ImageView pic;
        @Bind(R.id.discoverCourseBoxCourseTitle)
        TextView main;
        @Bind(R.id.discoverCourseBoxInstructorTitle)
        TextView aut;
        @Bind(R.id.discoverCourseBoxRatingBar)
        RatingBar bar;
        @Bind(R.id.discoverCourseBoxOriginalPrice)
        TextView price;
        @Bind(R.id.add_more)
        Button more;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.add)
        ImageView add;
        @Bind(R.id.remove)
        ImageView remove;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

