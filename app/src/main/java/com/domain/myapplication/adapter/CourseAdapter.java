package com.domain.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.BinderThread;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.domain.myapplication.CheckoutSingleton;
import com.domain.myapplication.Model.Checkout;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.R;
import com.domain.myapplication.activity.ProductDetailActivity;
import com.domain.myapplication.helper.ToFarsiChar;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CourseAdapter extends AbstractItem<CourseAdapter, CourseAdapter.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();
    CourseMain data;
    Context context;

    public CourseAdapter(CourseMain data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getType() {
        return R.id.fastadapter_radiobutton_sample_item_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.course;
    }


    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);


        Glide
                .with(context)
                .load(data.getUrl())
                .fitCenter()
                .into(holder.pic);


        if (data.getRibbon().equals("null") == false) {
            holder.ribbon.setVisibility(View.VISIBLE);
            holder.ribbon.setText(data.getRibbon());
        }


        holder.main.setText(data.getMatn());
        holder.aut.setText(ToFarsiChar.farsichar("موجودی " + data.getCount()));
        holder.bar.setRating(data.getTank());
        if (data.getStrikePrice() != null && !data.getStrikePrice().isEmpty()) {


            SpannableString text = new SpannableString(ToFarsiChar.farsichar(data.getStrikePrice() + " " + data.getPrice()));
            text.setSpan(new StrikethroughSpan(), 0, data.getStrikePrice().length(), 0);
            text.setSpan(new ForegroundColorSpan(context.getResources().getColor(android.R.color.holo_red_light)), 0, data.getStrikePrice().length(), 0);
            text.setSpan(new RelativeSizeSpan(.75f), 0, data.getStrikePrice().length(), 0);
            holder.price.setText(text, TextView.BufferType.SPANNABLE);
        } else {

            holder.price.setText(ToFarsiChar.farsichar(data.getPrice()));
        }
        Typeface type = Typeface.createFromAsset(context.getAssets(), "font/IRAN Sans Light.ttf");
        holder.main.setTypeface(type);
        holder.price.setTypeface(type);
        holder.aut.setTypeface(type);
        holder.bar.setDrawingCacheBackgroundColor(context.getResources().getColor(R.color.digikala_adrs_title1));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("item", data);
                context.startActivity(intent);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "به سبد خرید اضافه شد", Toast.LENGTH_SHORT).show();
                CheckoutSingleton.getInstance().add(data);
            }
        });

    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.discoverCourseBoxCourseImageView)
        ImageView pic;
        @Bind(R.id.discoverCourseBoxCourseTitle)
        TextView main;
        @Bind(R.id.discoverCourseBoxInstructorTitle)
        TextView aut;
        @Bind(R.id.discoverCourseBoxRatingBar)
        RatingBar bar;
        @Bind(R.id.discoverCourseBoxOriginalPrice)
        TextView price;
        @Bind(R.id.discoverCourseBoxDiscountRibbon)
        TextView ribbon;
        @Bind(R.id.add_more)
        Button more;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
