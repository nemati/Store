package com.domain.myapplication.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.R;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CatPrimaryAdapter extends AbstractItem<CatAdapter, CatPrimaryAdapter.ViewHolder> {

    CategoryMain data;
    Context context;
    Boolean animation = false;
    private static final int PHOTO_ANIMATION_DELAY = 600;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();

    public CatPrimaryAdapter(CategoryMain data, Context context, Boolean animation) {
        this.data = data;
        this.context = context;
        this.animation = animation;
    }

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();


    @Override
    public int getType() {
        return R.id.anishtan_layout_Category;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.grid_item_cat;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        int color = 0;
        try {
            color = Color.parseColor(data.getColor());
            holder.catCard.setCardBackgroundColor(color);
        } catch (Exception e) {
            e.getStackTrace();
            holder.catCard.setCardBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        }


        if (animation) {
            long animationDelay = PHOTO_ANIMATION_DELAY + holder.getPosition() * 30;
            holder.imagecat.setScaleY(0);
            holder.imagecat.setScaleX(0);

           ViewCompat.animate(holder.imagecat)
                    .scaleY(1)
                    .scaleX(1)
                    .setDuration(200)
                    .setInterpolator(INTERPOLATOR)
                    .setStartDelay(animationDelay)
                    .start();
            Log.e("animate", "started");
        }

        Glide
                .with(context)
                .load(data.getUrl())
                .fitCenter()
                .into(holder.imagecat);

    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.cat_card)
        CardView catCard;
        @Bind(R.id.cat_pic)
        ImageView imagecat;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


