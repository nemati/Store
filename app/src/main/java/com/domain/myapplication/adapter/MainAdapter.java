package com.domain.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.domain.myapplication.Model.BannerMain;
import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.Model.MainModel;
import com.domain.myapplication.R;
import com.domain.myapplication.activity.CategoryActivity;
import com.domain.myapplication.activity.ItemActivity;
import com.domain.myapplication.helper.SpacesItemDecoration;
import com.domain.myapplication.view.CountdownView;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class MainAdapter extends AbstractItem<MainAdapter, MainAdapter.ViewHolder> {

    private MainModel entity;
    private Context context;
    private FastItemAdapter rowDataAdapter;

    public MainAdapter(MainModel entity, Context context) {
        this.entity = entity;
        this.context = context;
    }

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    @Override
    public int getType() {
        return R.id.anishtan_layout_main;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);
        if (entity == null)
            return;
        rowDataAdapter = new FastItemAdapter();
        initviews(holder);
        drawNestedRecyclerView(holder);


    }

    void initviews(ViewHolder holder) {
        Typeface type = Typeface.createFromAsset(context.getAssets(), "font/IRAN Sans Light.ttf");


        if (entity.getDescription().equals("null") == false) {
            holder.description.setText(entity.getDescription());
            holder.description.setTypeface(type);
        } else {
            holder.description.setVisibility(View.GONE);
        }


        if (entity.getTitle().equals("null") == false) {
            holder.title.setText(entity.getTitle());
            holder.title.setTypeface(type);
        } else {
            holder.title.setVisibility(View.GONE);
        }


        if (entity.getMore().equals("null") == false) {
            holder.more.setText(entity.getMore());
            holder.more.setTypeface(type);
        } else {
            holder.more.setVisibility(View.GONE);
        }
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        holder.recyclerView.setNestedScrollingEnabled(false);
        holder.recyclerView.addItemDecoration(new SpacesItemDecoration(0));
        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setAdapter(rowDataAdapter);
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (entity.getTypeMain()) {
                    case CATEGORY:
                        Intent intent = new Intent(context, CategoryActivity.class);
                        context.startActivity(intent);
                        break;
                    case Course:
                        Intent intent2 = new Intent(context, ItemActivity.class);
                        context.startActivity(intent2);
                        break;

                }
            }
        });


    }


    void drawNestedRecyclerView(ViewHolder holder) {

        switch (entity.getTypeMain()) {
            case CATEGORY:
                drawCategoryRecyclerView(entity.getCategoryMains(), holder);
                break;
            case Course:
                drawCourseRecyclerView(entity.getCourseMains(), holder);
                break;
            case BANNER:
                drawBannerRecyclerView(entity.getBannerMains(), holder);
                break;


        }

    }


    void drawCategoryRecyclerView(List<CategoryMain> categoryMains, ViewHolder holder) {

        holder.recyclerView.addItemDecoration(new SpacesItemDecoration(0));
        for (int i = 0; i < categoryMains.size(); i++) {
            CatAdapter catAdapter = new CatAdapter(categoryMains.get(i), context);
            rowDataAdapter.add(catAdapter);
        }

    }

    void drawCourseRecyclerView(List<CourseMain> courseMains, ViewHolder holder) {
        holder.countdownView.setTag("coursetag");
        long time1 = (long) 5 * 60 * 60 * 1000;
        if (time1 / 86400000 > 1) {
            holder.countdownView.setShowDay(true);
        } else {
            holder.countdownView.setShowDay(false);
        }
        holder.countdownView.start(time1);
        holder.countdownView.setVisibility(View.VISIBLE);
        holder.recyclerView.addItemDecoration(new SpacesItemDecoration(10));
        for (int i = 0; i < courseMains.size(); i++) {
            CourseAdapter courseAdapter = new CourseAdapter(courseMains.get(i), context);
            rowDataAdapter.add(courseAdapter);
        }

    }

    void drawBannerRecyclerView(List<BannerMain> bannerMains, ViewHolder viewHolder) {
        viewHolder.recyclerView.setVisibility(View.GONE);
        viewHolder.recyclerView.addItemDecoration(new SpacesItemDecoration(0));
        BannerAdapter bannerAdapter = new BannerAdapter(bannerMains.get(0), context);
        rowDataAdapter.add(bannerAdapter);


    }


    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.anishtan_item_rv)
        RecyclerView recyclerView;
        @Bind(R.id.anishtan_item_tv_title)
        TextView title;
        @Bind(R.id.anishtan_item_tv_description)
        TextView description;
        @Bind(R.id.anishtan_item_btn_more)
        Button more;
        @Bind(R.id.anishtan_item_rel)
        RelativeLayout relativeLayout;
        @Bind(R.id.anishtan_item_countdown)
        CountdownView countdownView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
