package com.domain.myapplication.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.R;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CatAdapter extends AbstractItem<CatAdapter, CatAdapter.ViewHolder> {

    CategoryMain data;
    Context context;

    public CatAdapter(CategoryMain data, Context context) {
        this.data = data;
        this.context = context;
    }

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();


    @Override
    public int getType() {
        return R.id.anishtan_layout_Category;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.list_item_cat;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        int color = 0;
        try {
            color = Color.parseColor(data.getColor());
            holder.catCard.setCardBackgroundColor(color);
        } catch (Exception e) {
            e.getStackTrace();
            holder.catCard.setCardBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        }


        Glide
                .with(context)
                .load(data.getUrl())
                .fitCenter()

                .into(holder.imagecat);

    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.cat_card)
        CardView catCard;
        @Bind(R.id.cat_pic)
        ImageView imagecat;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
