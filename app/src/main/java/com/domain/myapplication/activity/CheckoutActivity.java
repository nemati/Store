package com.domain.myapplication.activity;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Toast;

import com.domain.myapplication.CheckoutSingleton;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.R;
import com.domain.myapplication.adapter.CourseGridAdapter;
import com.domain.myapplication.enums.TypeCourse;
import com.domain.myapplication.listener.RemoveCheckout;
import com.domain.myapplication.mvp.CheckoutMvp;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class CheckoutActivity extends AbstractActivity implements CheckoutMvp, RemoveCheckout {

    FloatingActionButton fab;


    private RecyclerView gridCourse;
    private RecyclerView lin;
    FastItemAdapter fastItemAdapter;
    FastItemAdapter adapterlin;
    Boolean isFirstList = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        fab = (FloatingActionButton) findViewById(R.id.done);
        lin = (RecyclerView) findViewById(R.id.productList_recyclerView_tileList);
        lin.setVisibility(View.VISIBLE);
        gridCourse = (RecyclerView) findViewById(R.id.productList_recyclerView_smallList);
        gridCourse.setVisibility(View.INVISIBLE);
        fastItemAdapter = new FastItemAdapter();
        adapterlin = new FastItemAdapter();
        gridCourse.setAdapter(fastItemAdapter);
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        lin.setLayoutManager(new LinearLayoutManager(this));
        lin.setAdapter(adapterlin);
        gridCourse.setLayoutManager(layoutManager);
        List<CourseMain> list2 = new ArrayList<>();
        for (CourseMain courseMain : CheckoutSingleton.getInstance().getItem()) {
            courseMain.setCount(1);
            list2.add(courseMain);
        }

        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), CheckoutActivity.this, TypeCourse.GRID, this);
            fastItemAdapter.add(adapter);
        }

        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), CheckoutActivity.this, TypeCourse.LINEAR, this);
            adapterlin.add(adapter);
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(CheckoutActivity.this, "خرید با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                CheckoutSingleton.getInstance().buy();
                CheckoutActivity.this.finish();

            }
        });
    }


    void preLilpopReveal() {
        if (isFirstList) {
            fab.setImageResource(R.drawable.ic_view_list_black_24dp);
            lin.setVisibility(View.VISIBLE);
            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = io.codetail.animation.ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), 0, finalRadius);
            toolbarContractAnim.setDuration(500);

            isFirstList = false;

            toolbarContractAnim.start();

        } else {
            fab.setImageResource(R.drawable.ic_list_black_24dp);

            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = io.codetail.animation.ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), finalRadius, 0);
            toolbarContractAnim.setDuration(600);

            toolbarContractAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    lin.setVisibility(View.INVISIBLE);
                    isFirstList = true;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            toolbarContractAnim.start();
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void lolipopReveal() {
        if (isFirstList) {
            fab.setImageResource(R.drawable.ic_view_list_black_24dp);
            lin.setVisibility(View.VISIBLE);

            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), 0, finalRadius);
            toolbarContractAnim.setDuration(500);

            isFirstList = false;

            toolbarContractAnim.start();
        } else {
            //  lin.setVisibility(View.INVISIBLE);
            fab.setImageResource(R.drawable.ic_list_black_24dp);
            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), finalRadius, 0);
            toolbarContractAnim.setDuration(500);

            toolbarContractAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    //   vUserProfileRoot.setVisibility(View.VISIBLE);
                    lin.setVisibility(View.INVISIBLE);
                    isFirstList = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            toolbarContractAnim.start();
        }

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void onItemRemoved(CourseMain courseMain) {

        Log.e("Here", "ere");
        CheckoutSingleton.getInstance().remove(courseMain);
        List<CourseMain> list2 = new ArrayList<>();
        for (CourseMain courseMain2 : CheckoutSingleton.getInstance().getItem()) {
            courseMain2.setCount(1);
            list2.add(courseMain2);
        }
        fastItemAdapter.clear();
        adapterlin.clear();
        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), CheckoutActivity.this, TypeCourse.GRID, this);
            fastItemAdapter.add(adapter);
        }

        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), CheckoutActivity.this, TypeCourse.LINEAR, this);
            adapterlin.add(adapter);
        }

    }
}
