package com.domain.myapplication.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.domain.myapplication.App;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.R;
import com.domain.myapplication.mvp.ProductDetailMvp;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class ProductDetailActivity extends AbstractActivity implements ProductDetailMvp {

    @Bind(R.id.slider)
    SliderLayout sliderLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);
        Typeface type = Typeface.createFromAsset(getAssets(), "font/IRAN Sans Light.ttf");
        TextView textView = (TextView) findViewById(R.id.detail);
        textView.setTypeface(type);
        initslider();
    }

    void initslider() {
        CourseMain courseMain = getIntent().getExtras().getParcelable("item");
        List<String> url = new ArrayList<String>();
        url.add(courseMain.getUrl());
        url.add(courseMain.getUrl());
        url.add(courseMain.getUrl());
        url.add(courseMain.getUrl());


        // initialize a SliderLayout


        for (int i = 0; i < url.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .description(courseMain.getMatn())
                    .image(url.get(i))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Toast.makeText(ProductDetailActivity.this, "روی اسلایدر کلیک شد!", Toast.LENGTH_SHORT).show();
                        }
                    });

            sliderLayout.addSlider(textSliderView);
        }


        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Stack);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void showProgress() {

    }
}
