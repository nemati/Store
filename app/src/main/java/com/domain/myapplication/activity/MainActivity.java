package com.domain.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.domain.myapplication.Model.BannerMain;
import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.Model.Checkout;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.Model.MainModel;
import com.domain.myapplication.R;
import com.domain.myapplication.adapter.BannerAdapter;
import com.domain.myapplication.adapter.MainAdapter;
import com.domain.myapplication.enums.TypeMain;
import com.domain.myapplication.helper.ToFarsiChar;
import com.domain.myapplication.mvp.MainMvp;
import com.domain.myapplication.mvp.presenter.MainPresenter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.itemanimators.AlphaCrossFadeAnimator;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class MainActivity extends AbstractActivity implements MainMvp {
    @Bind(R.id.slider)
    SliderLayout sliderLayout;
    @Bind(R.id.recycle_populate)
    RecyclerView catrv;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    ImageButton closeSearch;
    EditText searchText;
    Button textPish;
    RelativeLayout actionBar;
    Button menuOpen;
    Button search;
    Boolean isSearchViewShow;
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private FastItemAdapter mainAdapter;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        mainAdapter = new FastItemAdapter();
        setSupportActionBar(toolbar);

        Typeface type = Typeface.createFromAsset(getAssets(), "font/IRAN Sans Light.ttf");

        this.closeSearch = (ImageButton) findViewById(R.id.close_search);
        this.searchText = (EditText) findViewById(R.id.search_edittext2);
        this.textPish = (Button) findViewById(R.id.pishahang_text);
        this.actionBar = (RelativeLayout) findViewById(R.id.actionBar);
        this.menuOpen = (Button) findViewById(R.id.menu_open);
        this.search = (Button) findViewById(R.id.search);
        searchText.setTypeface(type);
        catrv.setLayoutManager(new LinearLayoutManager(this));
        catrv.setNestedScrollingEnabled(false);
        catrv.setHasFixedSize(true);
        catrv.setAdapter(mainAdapter);
        initslider();
        mainPresenter = new MainPresenter(this, this);
        mainPresenter.init();


        initdrawer();


    }


    void initdrawer() {
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "font/IRAN.ttf");

        final IProfile profile3 = new ProfileDrawerItem().withTypeface(myTypeface).withName("احمد نعمتی").withEmail("nematiprog@gmail.com").withIcon(R.drawable.profile2).withIdentifier(102);

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(

                        profile3,

                        //don't ask but google uses 14dp for the add account icon in gmail but 20dp for the normal icons (like manage account)
                        new ProfileSettingDrawerItem().withName("خروج").withTypeface(myTypeface).withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_plus).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text)).withIdentifier(10000)

                )


                .build();

        //Create the drawer
        DrawerBuilder builder = new DrawerBuilder();
        builder.withActivity(this);
        builder.withHasStableIds(true);
        builder.withItemAnimator(new AlphaCrossFadeAnimator());
        builder.withAccountHeader(headerResult);
        builder.addDrawerItems(


                new PrimaryDrawerItem().withTypeface(myTypeface).withName("سبد خرید").withIcon(FontAwesome.Icon.faw_buysellads).withIdentifier(2).withSelectable(false)


        ) // add the items we want to use with our Drawer
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        long i = drawerItem.getIdentifier();
                        if (i == 2) {
                            Intent intent = new Intent(MainActivity.this, CheckoutActivity.class);
                            startActivity(intent);
                        }

                        return false;
                    }
                })

                .withShowDrawerOnFirstLaunch(true)
                .withDrawerGravity(Gravity.RIGHT);


        result = builder.build();

        result.updateBadge(2, new StringHolder(ToFarsiChar.farsichar("0")));


        menuOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.openDrawer();
            }
        });
    }


    void initslider() {
        List<String> url = new ArrayList<String>();
        url.add("https://file.digi-kala.com/digikala/Image/Webstore/Banner/1396/11/25/e983aaf6.jpg");
        url.add("https://file.digi-kala.com/digikala/Image/Webstore/Banner/1396/11/25/ae489f46.jpg");
        url.add("https://file.digi-kala.com/digikala/Image/Webstore/Banner/1396/11/25/ea49ebd7.jpg");


        // initialize a SliderLayout


        for (int i = 0; i < url.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .description("فروش ویژه")
                    .image(url.get(i))
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Toast.makeText(MainActivity.this, "روی اسلایدر کلیک شد!", Toast.LENGTH_SHORT).show();
                        }
                    });

            sliderLayout.addSlider(textSliderView);
        }


        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Stack);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
        sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe()
    public void onMessageEvent(Checkout event) {
//        if (result != null)
//            if (event != null)
//                result.updateBadge(2, new StringHolder(ToFarsiChar.farsichar(String.valueOf(event.getCount()))));


    }

    ;


    @Override
    protected void onResume() {
        super.onResume();
        this.search.setBackgroundResource(R.drawable.search);
        this.actionBar.setBackgroundColor(0);
        this.menuOpen.setVisibility(View.VISIBLE);
        this.closeSearch.setVisibility(View.INVISIBLE);
        this.searchText.setVisibility(View.INVISIBLE);
        this.searchText.setText("");
        this.searchText.requestLayout();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButton();
            }
        });
    }

    public void searchButton() {
        final Button button = (Button) findViewById(R.id.zoom_shape);
        this.closeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKey();
            }
        });
        this.searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });
        button.setVisibility(View.VISIBLE);
        this.textPish.setVisibility(View.INVISIBLE);
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isSearchViewShow = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setVisibility(View.GONE);
                actionBar.setBackgroundColor(-1);
                searchText.setVisibility(View.VISIBLE);
                menuOpen.setVisibility(View.GONE);
                searchText.requestFocus();
                search.setBackgroundResource(R.drawable.magnify_sell);
                closeSearch.setVisibility(View.VISIBLE);
                showKey(searchText);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        button.startAnimation(loadAnimation);
    }

    public void showKey(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.showSoftInput(view, 0);
        }
    }

    public void hideKey() {
        actionBar.setBackgroundColor(0);
        search.setBackgroundResource(R.drawable.search);
        closeSearch.setVisibility(View.INVISIBLE);
        menuOpen.setVisibility(View.VISIBLE);
        searchText.setVisibility(View.INVISIBLE);

        searchText.setText("");
        searchText.requestLayout();
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        if (result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onError(String msg) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void data(List<CategoryMain> categoryMainList, List<CourseMain> courseMainList) {
        MainModel mainer2 = new MainModel("جشنواره نوروزی", "تا 80 درصد تخفیف دوره ها", "بیشتر", courseMainList, null, null, TypeMain.Course);
        MainAdapter sss2 = new MainAdapter(mainer2, this);


        BannerMain bannerMain = new BannerMain("https://udemy-images.udemy.com/course/750x422/289230_1056_16.jpg", "#FF93283F");
        BannerAdapter bannerAdapter = new BannerAdapter(bannerMain, this);


        BannerMain bannerMai2n = new BannerMain("https://udemy-images.udemy.com/course/750x422/8325_0f72_14.jpg", "#FF3098C8");
        BannerAdapter bannerAdapter2 = new BannerAdapter(bannerMai2n, this);


        MainModel mainer = new MainModel("دسته بندی ", "بقیه دسته بندی ها در بیشتر", "بیشتر", null, categoryMainList, null, TypeMain.CATEGORY);

        MainAdapter sss = new MainAdapter(mainer, this);


        mainAdapter.add(sss2);

        //    mainAdapter.add(sss);
        mainAdapter.add(sss2);
        mainAdapter.add(bannerAdapter);
        mainAdapter.add(sss2);
        mainAdapter.add(bannerAdapter2);
    }
}
