package com.domain.myapplication.activity;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.R;
import com.domain.myapplication.adapter.CatPrimaryAdapter;
import com.domain.myapplication.helper.ToFarsiChar;
import com.domain.myapplication.mvp.CategoryMvp;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.iconics.view.IconicsImageView;

import java.util.ArrayList;
import java.util.List;


public class CategoryActivity extends AbstractActivity implements CategoryMvp {

    Toolbar toolbar;
    Boolean animation = false;
    RecyclerView view;
    RecyclerView recyclerView;
    LinearLayout stats;
    TextView txt1, txt2, txt3;
    IconicsImageView imageView, imageview2, imageview3;
    View collapsing_toolbar;
    View vUserProfileRoot;
    FastItemAdapter fastItemAdapter;

    View coordinatorLayout;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        view = (RecyclerView) findViewById(R.id.rvUserProfile);
        fastItemAdapter = new FastItemAdapter();
        recyclerView = (RecyclerView) findViewById(R.id.rvUserProfile);
        setupUserProfileGrid();
        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        vUserProfileRoot = findViewById(R.id.vUserProfileRoot);
        vUserProfileRoot.setVisibility(View.INVISIBLE);


        txt1.setTypeface(getTypeFace());
        txt2.setTypeface(getTypeFace());
        txt3.setTypeface(getTypeFace());
        txt1.setText(ToFarsiChar.farsichar("1000+ ساعت تدریس"));
        txt2.setText(ToFarsiChar.farsichar("5000+ دانش آموخته"));
        txt3.setText(ToFarsiChar.farsichar("500+ مدرس"));
        imageView = (IconicsImageView) findViewById(R.id.material_drawer_arrow);
        imageview2 = (IconicsImageView) findViewById(R.id.material_drawer_arrow2);
        imageview3 = (IconicsImageView) findViewById(R.id.material_drawer_arrow3);
        Glide.with(this).load(R.drawable.clock).fitCenter().into(imageView);
        Glide.with(this).load(R.drawable.student).fitCenter().into(imageview2);
        Glide.with(this).load(R.drawable.tech).fitCenter().into(imageview3);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        stats = (LinearLayout) findViewById(R.id.vUserStats);
        setSupportActionBar(toolbar);
    }

    private void animateUserProfileHeader() {


        ViewCompat.animate(stats).translationY(0).alpha(1).setDuration(800).setStartDelay(100).setListener(new ViewPropertyAnimatorListener() {
            @Override
            public void onAnimationStart(View view) {

            }

            @Override
            public void onAnimationEnd(View view) {
                drawRecycler();
                Log.e("animation", "end");
            }

            @Override
            public void onAnimationCancel(View view) {

            }
        }).setInterpolator(INTERPOLATOR).start();
//        ViewCompat.animate(vUserDetails).translationY(0).setDuration(300).setStartDelay(200).setInterpolator(INTERPOLATOR).start();
//        ViewCompat.animate(vUserStats).alpha(1).setDuration(200).setStartDelay(400).setInterpolator(INTERPOLATOR).start();
    }

    private void setupUserProfileGrid() {

        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(fastItemAdapter);
        recyclerView.setHasFixedSize(false);


    }


    void drawRecycler() {
        List<CategoryMain> list = new ArrayList<CategoryMain>();
        CategoryMain main = new CategoryMain("#E55801", "http://faradars.org/wp-content/uploads/2013/12/icon-programming.png");
        CategoryMain main2 = new CategoryMain("#E89704", "http://faradars.org/wp-content/uploads/2016/01/oloom.png");
        CategoryMain main3 = new CategoryMain("#2E0BAA", "http://faradars.org/wp-content/uploads/2016/01/olumtajrobi.png");
        CategoryMain main4 = new CategoryMain("#E70E83", "http://faradars.org/wp-content/uploads/2013/12/icon-general.png");
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        for (int i = 0; i < list.size(); i++) {
            CatPrimaryAdapter categoryActivity = new CatPrimaryAdapter(list.get(i), this, true);
            fastItemAdapter.add(categoryActivity);
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus || animation)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lolipopReveal();
            return;
        }
        preLilpopReveal();


    }

    void preLilpopReveal() {

        coordinatorLayout.setVisibility(View.VISIBLE);
        int cx = (coordinatorLayout.getLeft() + coordinatorLayout.getRight());
        int cy = (coordinatorLayout.getTop() + coordinatorLayout.getBottom());
        int dx = Math.max(cx, coordinatorLayout.getWidth() - cx);
        int dy = Math.max(cy, coordinatorLayout.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);
        final Animator toolbarContractAnim = io.codetail.animation.ViewAnimationUtils
                .createCircularReveal(coordinatorLayout, coordinatorLayout.getRight(), coordinatorLayout.getTop(), 0, finalRadius);
        toolbarContractAnim.setDuration(600);

        toolbarContractAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                vUserProfileRoot.setVisibility(View.VISIBLE);
                stats.setTranslationY(-200);
                stats.setAlpha(0);
                animateUserProfileHeader();
                animation = true;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        toolbarContractAnim.start();
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void lolipopReveal() {
        coordinatorLayout.setVisibility(View.VISIBLE);
        int cx = (coordinatorLayout.getLeft() + coordinatorLayout.getRight());
        int cy = (coordinatorLayout.getTop() + coordinatorLayout.getBottom());
        int dx = Math.max(cx, coordinatorLayout.getWidth() - cx);
        int dy = Math.max(cy, coordinatorLayout.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);
        final Animator toolbarContractAnim = ViewAnimationUtils
                .createCircularReveal(coordinatorLayout, coordinatorLayout.getRight(), coordinatorLayout.getTop(), 0, finalRadius);
        toolbarContractAnim.setDuration(600);

        toolbarContractAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                vUserProfileRoot.setVisibility(View.VISIBLE);
                stats.setTranslationY(-200);
                stats.setAlpha(0);
                animateUserProfileHeader();
                animation = true;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        toolbarContractAnim.start();

    }


    @Override
    public void onError(String msg) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void showProgress() {

    }
}



