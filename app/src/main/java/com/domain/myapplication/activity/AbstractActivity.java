package com.domain.myapplication.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by IT.guest on 2/21/2018.
 */

public abstract class AbstractActivity extends AppCompatActivity {

    protected Typeface getTypeFace()
    {
       return Typeface.createFromAsset(getAssets(), "font/IRAN Sans Light.ttf");
    }
}
