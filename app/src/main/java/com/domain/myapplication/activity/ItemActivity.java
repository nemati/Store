package com.domain.myapplication.activity;


import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.R;
import com.domain.myapplication.adapter.CourseGridAdapter;
import com.domain.myapplication.enums.TypeCourse;
import com.domain.myapplication.mvp.ItemMvp;
import com.domain.myapplication.mvp.presenter.ItemPresenter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class ItemActivity extends AbstractActivity implements ItemMvp {
    FloatingActionButton fab;


    private RecyclerView gridCourse;
    private RecyclerView lin;
    FastItemAdapter fastItemAdapter;
    FastItemAdapter adapterlin;
    Boolean isFirstList = true;
    private ItemPresenter itemPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        fab = (FloatingActionButton) findViewById(R.id.fabButton);
        lin = (RecyclerView) findViewById(R.id.productList_recyclerView_tileList);
        lin.setVisibility(View.INVISIBLE);
        gridCourse = (RecyclerView) findViewById(R.id.productList_recyclerView_smallList);
        fastItemAdapter = new FastItemAdapter();
        adapterlin = new FastItemAdapter();
        gridCourse.setAdapter(fastItemAdapter);
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        lin.setLayoutManager(new LinearLayoutManager(this));
        lin.setAdapter(adapterlin);
        gridCourse.setLayoutManager(layoutManager);
        itemPresenter = new ItemPresenter(this, this);
        itemPresenter.init();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    lolipopReveal();
                    return;
                }

                preLilpopReveal();


            }
        });
    }


    void preLilpopReveal() {
        if (isFirstList) {
            fab.setImageResource(R.drawable.ic_view_list_black_24dp);
            lin.setVisibility(View.VISIBLE);
            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = io.codetail.animation.ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), 0, finalRadius);
            toolbarContractAnim.setDuration(500);

            isFirstList = false;

            toolbarContractAnim.start();

        } else {
            fab.setImageResource(R.drawable.ic_list_black_24dp);

            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = io.codetail.animation.ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), finalRadius, 0);
            toolbarContractAnim.setDuration(600);

            toolbarContractAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    lin.setVisibility(View.INVISIBLE);
                    isFirstList = true;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            toolbarContractAnim.start();
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void lolipopReveal() {
        if (isFirstList) {
            fab.setImageResource(R.drawable.ic_view_list_black_24dp);
            lin.setVisibility(View.VISIBLE);

            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), 0, finalRadius);
            toolbarContractAnim.setDuration(500);

            isFirstList = false;

            toolbarContractAnim.start();
        } else {
            //  lin.setVisibility(View.INVISIBLE);
            fab.setImageResource(R.drawable.ic_list_black_24dp);
            int cx = (lin.getLeft() + lin.getRight());
            int cy = (lin.getTop() + lin.getBottom());
            int dx = Math.max(cx, lin.getWidth() - cx);
            int dy = Math.max(cy, lin.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);
            final Animator toolbarContractAnim = ViewAnimationUtils
                    .createCircularReveal(lin, lin.getLeft(), lin.getBottom(), finalRadius, 0);
            toolbarContractAnim.setDuration(500);

            toolbarContractAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    //   vUserProfileRoot.setVisibility(View.VISIBLE);
                    lin.setVisibility(View.INVISIBLE);
                    isFirstList = true;

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            toolbarContractAnim.start();
        }

    }


    @Override
    public void onError(String msg) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void data(List<CourseMain> list2) {
        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), ItemActivity.this, TypeCourse.GRID);
            fastItemAdapter.add(adapter);
        }

        for (int i = 0; i < list2.size(); i++) {
            CourseGridAdapter adapter = new CourseGridAdapter(list2.get(i), ItemActivity.this, TypeCourse.LINEAR);
            adapterlin.add(adapter);
        }
    }
}
