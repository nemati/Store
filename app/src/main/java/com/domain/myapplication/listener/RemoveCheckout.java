package com.domain.myapplication.listener;

import com.domain.myapplication.Model.CourseMain;

/**
 * Created by IT.guest on 2/21/2018.
 */

public interface RemoveCheckout {

    void onItemRemoved(CourseMain courseMain);
}
