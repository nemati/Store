package com.domain.myapplication;

import com.domain.myapplication.fake_Data.FakeCategoryData;
import com.domain.myapplication.fake_Data.FakeCourseData;
import com.orm.SugarApp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class App extends SugarApp {

    public static void fillData() {
        FakeCourseData.fill();
        FakeCategoryData.fill();
    }
}
