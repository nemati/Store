package com.domain.myapplication.Model;

import com.domain.myapplication.enums.TypeMain;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class MainModel extends SugarRecord {
    String title;
    String description;
    String more;
    List<CourseMain> courseMains;
    List<CategoryMain> categoryMains;
    List<BannerMain> bannerMains;
    TypeMain typeMain;

    public String getTitle() {
        return title;
    }

    public List<BannerMain> getBannerMains() {
        return bannerMains;
    }

    public TypeMain getTypeMain() {
        return typeMain;
    }

    public String getDescription() {
        return description;
    }

    public List<CourseMain> getCourseMains() {
        return courseMains;
    }

    public List<CategoryMain> getCategoryMains() {
        return categoryMains;
    }

    public String getMore() {
        return more;

    }


    public MainModel(String title, String description, String more, List<CourseMain> data, List<CategoryMain> categoryMains, List<BannerMain> bannerMains, TypeMain typeMain) {
        this.title = title;
        this.description = description;
        this.more = more;
        this.courseMains = data;
        this.categoryMains = categoryMains;
        this.typeMain = typeMain;
        this.bannerMains = bannerMains;
    }
}
