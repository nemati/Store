package com.domain.myapplication.Model;

import com.orm.SugarRecord;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class BannerMain extends SugarRecord {
    String urlPic;
    String color;

    public String getUrlPic() {
        return urlPic;
    }

    public String getColor() {
        return color;
    }

    public BannerMain(String urlPic, String color) {

        this.urlPic = urlPic;
        this.color = color;
    }
}
