package com.domain.myapplication.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CourseMain extends SugarRecord implements Parcelable {

    private String url;
    private String matn;
    private int count;
    private int tank;
    private String price;

    protected CourseMain(Parcel in) {
        url = in.readString();
        matn = in.readString();
        count = in.readInt();
        tank = in.readInt();
        price = in.readString();
        strikePrice = in.readString();
        ribbon = in.readString();
    }

    public static final Creator<CourseMain> CREATOR = new Creator<CourseMain>() {
        @Override
        public CourseMain createFromParcel(Parcel in) {
            return new CourseMain(in);
        }

        @Override
        public CourseMain[] newArray(int size) {
            return new CourseMain[size];
        }
    };

    public String getStrikePrice() {
        return strikePrice;
    }

    private String strikePrice;


    public CourseMain(String url, String matn, int count, int tank, String price, String ribbon, String strikePrice) {
        this.url = url;
        this.matn = matn;
        this.count = count;
        this.tank = tank;
        this.price = price;
        this.ribbon = ribbon;
        this.strikePrice = strikePrice;
    }

    public String getUrl() {
        return url;
    }

    public String getMatn() {
        return matn;
    }

    public String getCount() {
        return String.valueOf(count);
    }

    public int getTank() {
        return tank;
    }

    public String getPrice() {
        return price;
    }

    public String getRibbon() {
        return ribbon;
    }

    private String ribbon;


    @Override
    public int describeContents() {
        return 0;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMatn(String matn) {
        this.matn = matn;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setTank(int tank) {
        this.tank = tank;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setStrikePrice(String strikePrice) {
        this.strikePrice = strikePrice;
    }

    public void setRibbon(String ribbon) {
        this.ribbon = ribbon;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeString(matn);
        parcel.writeInt(count);
        parcel.writeInt(tank);
        parcel.writeString(price);
        parcel.writeString(strikePrice);
        parcel.writeString(ribbon);
    }
}
