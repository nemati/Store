package com.domain.myapplication.Model;

import com.orm.SugarRecord;

/**
 * Created by IT.guest on 2/21/2018.
 */
public class CategoryMain extends SugarRecord {

    private String color;

    public String getColor() {
        return color;
    }

    public String getUrl() {
        return url;
    }

    public CategoryMain(String color, String url) {

        this.color = color;
        this.url = url;
    }

    private String url;
}
