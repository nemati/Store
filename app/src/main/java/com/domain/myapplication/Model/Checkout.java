package com.domain.myapplication.Model;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class Checkout {
    int count;

    public Checkout(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
