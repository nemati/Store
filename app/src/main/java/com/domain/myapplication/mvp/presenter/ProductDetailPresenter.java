package com.domain.myapplication.mvp.presenter;

import com.domain.myapplication.activity.ProductDetailActivity;
import com.domain.myapplication.mvp.ProductDetailMvp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class ProductDetailPresenter extends BasePresenter<ProductDetailActivity,ProductDetailMvp> {

    public ProductDetailPresenter(ProductDetailActivity activity, ProductDetailMvp mvp) {
        super(activity, mvp);
    }
}
