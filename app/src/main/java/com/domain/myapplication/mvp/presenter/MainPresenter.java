package com.domain.myapplication.mvp.presenter;

import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.activity.MainActivity;
import com.domain.myapplication.mvp.BaseMvp;
import com.domain.myapplication.mvp.MainMvp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class MainPresenter extends BasePresenter<MainActivity, MainMvp> {


    public MainPresenter(MainActivity activity, MainMvp mvp) {
        super(activity, mvp);
    }

    public void init() {
        List<CategoryMain> list = new ArrayList<CategoryMain>();
        List<CourseMain> list2 = new ArrayList<>();
        CategoryMain main = new CategoryMain("#E55801", "http://faradars.org/wp-content/uploads/2013/12/icon-programming.png");
        CategoryMain main2 = new CategoryMain("#E89704", "http://faradars.org/wp-content/uploads/2016/01/oloom.png");
        CategoryMain main3 = new CategoryMain("#2E0BAA", "http://faradars.org/wp-content/uploads/2016/01/olumtajrobi.png");
        CategoryMain main4 = new CategoryMain("#E70E83", "http://faradars.org/wp-content/uploads/2013/12/icon-general.png");
        list.add(main);
        list.add(main2);
        list.add(main3);
        list.add(main4);

        CourseMain coursemodel = new CourseMain("https://file.digi-kala.com/digikala/image/webstore/product/p_288306/220/apple-ipad-9-7-inch-2017-wifi-32gb-tablet-bfe395.jpg", "تبلت اپل مدل iPad 9.7 inch (2017) WiFi ", 5, 5, "500 تومان", "10% تخفیف", "1000 تومان");
        CourseMain coursemode2 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_279154/350/Reolink-Argus-Network-Camera-dc2286.jpg", "دوربين تحت شبکه ريولينک مدل Argus", 4, 4, "100 تومان", "20% تخفیف", "1000 تومان");
        CourseMain coursemode3 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_431544/350/Sony-Playstation-4-Pro-Region-2-CUH-7016B-1TB-Game-Console-7eef7e.jpg", "مجموعه کنسول بازی سونی مدل Playstation 4 Pro", 10, 3, "2000 تومان", "null", null);
        CourseMain coursemode4 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_385889/350/babyliss-6613-Hair-Dryer-b1717a.jpg", "سشوار بابيليس مدل 6613DE", 20, 2, "3000 تومان", "null", null);
        list2.add(coursemodel);
        list2.add(coursemode2);
        list2.add(coursemode3);
        list2.add(coursemode4);

        mvp.data(list, list2);


    }


}
