package com.domain.myapplication.mvp;

import com.domain.myapplication.Model.CategoryMain;
import com.domain.myapplication.Model.CourseMain;

import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public interface MainMvp extends BaseMvp {

    void data (List<CategoryMain> list, List<CourseMain> list2);

}
