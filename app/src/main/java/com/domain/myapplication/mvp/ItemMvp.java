package com.domain.myapplication.mvp;

import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.mvp.BaseMvp;

import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public interface ItemMvp extends BaseMvp {

    void data(List<CourseMain> courseMainList);
}
