package com.domain.myapplication.mvp.presenter;

import com.domain.myapplication.activity.CheckoutActivity;
import com.domain.myapplication.mvp.CheckoutMvp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class CheckoutPresenter extends BasePresenter<CheckoutActivity,CheckoutMvp> {


    public CheckoutPresenter(CheckoutActivity activity, CheckoutMvp mvp) {
        super(activity, mvp);
    }
}
