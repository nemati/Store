package com.domain.myapplication.mvp.presenter;

import com.domain.myapplication.activity.CategoryActivity;
import com.domain.myapplication.mvp.CategoryMvp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class CategoryPresenter extends BasePresenter<CategoryActivity,CategoryMvp> {


    public CategoryPresenter(CategoryActivity activity, CategoryMvp mvp) {
        super(activity, mvp);
    }
}
