package com.domain.myapplication.mvp.presenter;

import android.support.v7.app.AppCompatActivity;

import com.domain.myapplication.mvp.BaseMvp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public abstract class BasePresenter<T extends AppCompatActivity, E extends BaseMvp> {

    private T activity;
    public E mvp;

    public BasePresenter(T activity, E mvp) {
        this.activity = activity;
        this.mvp = mvp;
    }
}
