package com.domain.myapplication.mvp.presenter;

import com.domain.myapplication.Model.CourseMain;
import com.domain.myapplication.activity.ItemActivity;
import com.domain.myapplication.mvp.ItemMvp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class ItemPresenter extends BasePresenter<ItemActivity, ItemMvp> {

    public ItemPresenter(ItemActivity activity, ItemMvp mvp) {
        super(activity, mvp);
    }


    public void init() {
        List<CourseMain> list2 = new ArrayList<>();
        CourseMain coursemodel = new CourseMain("https://file.digi-kala.com/digikala/image/webstore/product/p_288306/220/apple-ipad-9-7-inch-2017-wifi-32gb-tablet-bfe395.jpg", "تبلت اپل مدل iPad 9.7 inch (2017) WiFi ", 5, 5, "500 تومان", "10% تخفیف", "1000 تومان");
        CourseMain coursemode2 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_279154/350/Reolink-Argus-Network-Camera-dc2286.jpg", "دوربين تحت شبکه ريولينک مدل Argus", 4, 4, "100 تومان", "20% تخفیف", "1000 تومان");
        CourseMain coursemode3 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_431544/350/Sony-Playstation-4-Pro-Region-2-CUH-7016B-1TB-Game-Console-7eef7e.jpg", "مجموعه کنسول بازی سونی مدل Playstation 4 Pro", 10, 3, "2000 تومان", "null", null);
        CourseMain coursemode4 = new CourseMain("http://file.digi-kala.com/digikala/Image/Webstore/Product/P_385889/350/babyliss-6613-Hair-Dryer-b1717a.jpg", "سشوار بابيليس مدل 6613DE", 20, 2, "3000 تومان", "null", null);
        list2.add(coursemodel);
        list2.add(coursemode2);
        list2.add(coursemode3);
        list2.add(coursemode4);
        mvp.data(list2);
    }
}
