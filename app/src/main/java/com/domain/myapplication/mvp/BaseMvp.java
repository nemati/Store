package com.domain.myapplication.mvp;

/**
 * Created by IT.guest on 2/21/2018.
 */

public interface BaseMvp {
    void onError(String msg);

    void kill();

    void showProgress();

}

