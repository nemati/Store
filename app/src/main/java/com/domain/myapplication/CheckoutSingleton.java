package com.domain.myapplication;

import com.domain.myapplication.Model.Checkout;
import com.domain.myapplication.Model.CourseMain;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IT.guest on 2/21/2018.
 */

public class CheckoutSingleton {
    private List<CourseMain> courseMains;
    private static final CheckoutSingleton ourInstance = new CheckoutSingleton();

    public static CheckoutSingleton getInstance() {
        return ourInstance;
    }

    private CheckoutSingleton() {
        if (courseMains == null)
            courseMains = new ArrayList<>();
    }


    public void add(CourseMain courseMain) {

        courseMains.add(courseMain);
        broadcastItem();
    }

    private void broadcastItem() {
        EventBus.getDefault().post(new Checkout(courseMains.size()));
    }

    public List<CourseMain> getItem() {
        return courseMains;
    }

    public void buy() {
        courseMains.clear();
        broadcastItem();
    }

    public void remove(CourseMain courseMain) {
        for (int i = 0; i < courseMains.size(); i++) {
            if (courseMains.get(i).getUrl().equals(courseMain.getUrl()))
                courseMains.remove(i);
        }
    }


}
